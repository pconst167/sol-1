<html>
<title>Sol-1 74 Series Logic Homebrew CPU - 2.5MHz</title>
	
<head>
<meta name="keywords" content="homebrew cpu,homebuilt cpu,ttl,alu,homebuilt computer,homebrew computer,74181,Sol-1, Sol, electronics, hardware, engineering, programming, assembly, cpu, logic">
<link rel="icon" href="http://sol-1.org/images/2.jpg">	

</head>

<body>


<?php
	include("menu.php");
?>
<header>
<h3><span id="headerSubTitle">videos</span></h3>
</header>


<table>
	<tr>
		<td align="center">
			<iframe style="border-radius:10px;" width="300" height="300" src="https://www.youtube.com/embed/LQJOD-YyRi8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</td>
		<td align="center">
			<iframe  style="border-radius:10px;"width="300" height="300" src="https://www.youtube.com/embed/-mnIOoQ9Jxo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</td>
		<td align="center">
			<iframe style="border-radius:10px;" width="300" height="300" src="https://www.youtube.com/embed/Q-ZfEQytvYs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</td>
	</tr>
	<tr>
		
		<td align="center">
			<iframe style="border-radius:10px;" width="300" height="300" src="https://www.youtube.com/embed/xK2rUuUZaso" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</td>
		<td align="center">
			<iframe style="border-radius:10px;" width="300" height="300" src="https://www.youtube.com/embed/1ELm-5UGSP0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</td>
		<td align="center">
			<iframe style="border-radius:10px;" width="300" height="300" src="https://www.youtube.com/embed/Tx7uD30wfeg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</td>
	</tr>
	
	
</table>



</body>
</html>

